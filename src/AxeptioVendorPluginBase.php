<?php

namespace Drupal\axeptio;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for axeptio_vendor plugins.
 */
abstract class AxeptioVendorPluginBase extends PluginBase implements AxeptioVendorInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function isSourceIsConcerned($source) {
    foreach ($this->getPaterns() as $patern) {
      if (preg_match($patern, $source)) {
        return static::RESTRICTED;
      }
    }
    return static::NEUTRAL;
  }

  /**
   * {@inheritdoc}
   */
  public function getVendorName($source) {
    return $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultAction() {
    return static::RESTRICTED;
  }

}
