<?php

namespace Drupal\axeptio;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Axeptio main service.
 */
class Axeptio implements AxeptioInterface {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Axeptio config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new Axeptio object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;

    $this->config = $configFactory->get('axeptio.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->config->get('id');
  }

  /**
   * {@inheritdoc}
   */
  public function isLanguageSpecific() {
    return (bool) $this->config->get('language_specific');
  }

  /**
   * {@inheritdoc}
   */
  public function getCookiesVersion($admin = FALSE) {
    $cookies_version = $this->config->get('cookies_version');
    if ($admin || !$this->isLanguageSpecific()) {
      return $cookies_version;
    }

    $language = $this->languageManager->getCurrentLanguage();
    return $cookies_version . '-' . $language->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCookiesDuration() {
    return (int) $this->config->get('cookies_duration');
  }

  /**
   * {@inheritdoc}
   */
  public function getCookiesSecure() {
    return (bool) $this->config->get('cookies_secure');
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    if ($this->getId()) {
      if ($this->isLanguageSpecific()) {
        return (bool) $this->getCookiesVersion();
      }
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isConsentModeV2IsEnable() {
    return (int) $this->config->get('consent_mode_v2');
  }

  /**
   * {@inheritdoc}
   */
  public function getConsentModeV2AnalyticsStorage() {
    return (int) $this->config->get('consent_mode_v2_analytics_storage');
  }

  /**
   * {@inheritdoc}
   */
  public function getConsentModeV2AdStorage() {
    return (int) $this->config->get('consent_mode_v2_ad_storage');
  }

  /**
   * {@inheritdoc}
   */
  public function getConsentModeV2AdUserData() {
    return (int) $this->config->get('consent_mode_v2_ad_user_data');
  }

  /**
   * {@inheritdoc}
   */
  public function getConsentModeV2AdPersonalization() {
    return (int) $this->config->get('consent_mode_v2_ad_personalization');
  }

}
