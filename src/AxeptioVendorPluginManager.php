<?php

namespace Drupal\axeptio;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * AxeptioVendor plugin manager.
 */
class AxeptioVendorPluginManager extends DefaultPluginManager implements AxeptioVendorPluginManagerInterface {

  /**
   * Array of Axeptio plugin instance.
   *
   * @var \Drupal\axeptio\AxeptioVendorInterface[]
   */
  public $instances = [];

  /**
   * Constructs a new AxeptioVendorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/AxeptioVendor',
      $namespaces,
      $module_handler,
      'Drupal\axeptio\AxeptioVendorInterface',
      'Drupal\axeptio\Annotation\AxeptioVendor'
    );
    $this->alterInfo('axeptio_vendor_info');
    $this->setCacheBackend($cache_backend, 'axeptio_vendor_plugins');
  }

  /**
   * Return the list of plugins.
   *
   * @return array
   *   Array of plugins.
   */
  public function getPlugins() {
    $plugins = [];
    foreach ($this->getDefinitions() as $definition) {
      $plugins[$definition['id']] = $this->getInstance($definition);
    }
    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'unknown';
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPlugin() {
    $id = $this->getFallbackPluginId('fallback');
    $plugins = $this->getPlugins();

    return $plugins[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    if (empty($this->instances[$options['id']])) {
      $this->instances[$options['id']] = $this->createInstance($options['id'], $options);
    }
    return $this->instances[$options['id']];
  }

}
