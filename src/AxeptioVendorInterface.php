<?php

namespace Drupal\axeptio;

/**
 * Interface for axeptio_vendor plugins.
 */
interface AxeptioVendorInterface {

  const RESTRICTED = 1;

  const NOT_CONCERNED = 2;

  const NEUTRAL = 0;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Return the source pattern.
   *
   * @return array
   *   Array of patterns.
   */
  public function getPaterns();

  /**
   * Return the plugin vendor name.
   *
   * @param string $source
   *   The source string.
   *
   * @return string
   *   The vendor name.
   */
  public function getVendorName($source);

  /**
   * Return the default action when the plugin is concerned.
   *
   * @return int
   *   The default action int.
   */
  public function getDefaultAction();

  /**
   * Checks if the content.
   *
   * @param string $source
   *   The source string.
   *
   * @return bool
   *   True if the plugin is concerned.
   */
  public function isSourceIsConcerned($source);

}
