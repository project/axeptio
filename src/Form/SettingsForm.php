<?php

namespace Drupal\axeptio\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Axeptio settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->client = $container->get('http_client');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['axeptio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'axeptio_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('axeptio.settings');

    // Get the current Axeptio ID.
    $stored_id = $config->get('id');
    $id = $form_state->getValue('id');
    if (!$id) {
      $id = $stored_id;
    }

    $form['settings'] = ['#type' => 'container'];

    $form['settings']['#prefix'] = '<div id="axeptio_settings">';
    $form['settings']['#suffix'] = '</div>';

    $form['settings']['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project id'),
      '#default_value' => $config->get('id'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxRefreshForm'],
        'wrapper' => 'axeptio_settings',
        'disable-refocus' => TRUE,
      ],
    ];

    $form['settings']['cookies_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Cookies duration'),
      '#description' => $this->t('The max life duration in day of the cookies. Watch out for the DLC! Cookies are 13 months no more.'),
      '#default_value' => $config->get('cookies_duration'),
      '#required' => TRUE,
    ];

    $form['settings']['cookies_secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cookies secure'),
      '#description' => $this->t('Wether or not the cookie holding choices is HTTPS only.'),
      '#default_value' => $config->get('cookies_secure'),
    ];

    $form['settings']['language_specific'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cookies language specific'),
      '#description' => $this->t('If checked, the version cookies will be completed with the language code like this: xxxxx-en'),
      '#default_value' => $config->get('language_specific'),
    ];

    $options = $id ? $this->getProjectFromAxeptio($id) : [];
    $form['settings']['cookies_version'] = [
      '#type' => 'select',
      '#title' => $this->t('Cookies version'),
      '#default_value' => $stored_id != $id ? reset($options) : $config->get('cookies_version'),
      '#options' => $options,
      '#states' => [
        'required' => [
          ':input[name="language_specific"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Restrict the duration to 390 days maximum.
    if ($form_state->getValue('cookies_duration') > 390) {
      $form_state->setError($form['cookies_duration'], $this->t('The max duration fo Cookies is 13 months, no more'));
    }
  }

  /**
   * Ajax callback.
   */
  public function ajaxRefreshForm(array &$form, FormStateInterface $form_state) {
    return $form['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fields = [
      'id',
      'cookies_version',
      'cookies_duration',
      'cookies_secure',
      'language_specific',
    ];

    $config = $this->config('axeptio.settings');
    foreach ($fields as $field) {
      $config->set($field, $form_state->getValue($field));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get cookies information from Axeptio endpoint.
   *
   * @param string $id
   *   The Axeptio ID.
   *
   * @return array
   *   The list of cookies name available.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function getProjectFromAxeptio(string $id) {
    $response = $this->client->get("https://client.axept.io/$id.json");

    $options = [];
    if ($response->getStatusCode() === 200) {
      $json = Json::decode($response->getBody());

      if (!empty($json['cookies'])) {
        foreach ($json['cookies'] as $cookie) {
          $options[$cookie['name']] = $cookie['name'];
        }
      }
    }

    return $options;
  }

}
