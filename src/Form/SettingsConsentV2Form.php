<?php

namespace Drupal\axeptio\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Axeptio consent mode settings form.
 */
class SettingsConsentV2Form extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['axeptio.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'axeptio_settings_consent_v2_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('axeptio.settings');

    $form['consent_mode_v2'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Google Consent Mode V2?'),
      '#default_value' => $config->get('consent_mode_v2'),
    ];

    $form['consent_mode_v2_description'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['axeptio-description']],
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_v2"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['consent_mode_v2_description']['title'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['axeptio-description-title']],
      '#markup' => $this->t('Default setting for Consent Mode'),
    ];

    $form['consent_mode_v2_description']['description'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['axeptio-description-description']],
      '#markup' => $this->t("These consent signals will be sent when the page is loaded to tell Google's services how they should process the data before consent is given by the user."),
    ];

    $form['consent_mode_v2_options'] = [
      '#type' => 'fieldset',
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_v2"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['consent_mode_v2_options']['consent_mode_v2_analytics_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Analytics storage'),
      '#description' => $this->t('Authorise Google Analytics to measure how visitors use the site in order to improve functionality and service.'),
      '#default_value' => $config->get('consent_mode_v2_analytics_storage'),
    ];

    $form['consent_mode_v2_options']['consent_mode_v2_ad_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ad storage'),
      '#description' => $this->t("Allows Google to store advertising information on visitors' devices to improve the relevance of ads."),
      '#default_value' => $config->get('consent_mode_v2_ad_storage'),
    ];

    $form['consent_mode_v2_options']['consent_mode_v2_ad_user_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ad User Data'),
      '#description' => $this->t('Share visitor activity data with Google for targeted advertising.'),
      '#default_value' => $config->get('consent_mode_v2_ad_user_data'),
    ];

    $form['consent_mode_v2_options']['consent_mode_v2_ad_personalization'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ad Personalization'),
      '#description' => $this->t('Personalise the advertising experience by enabling Google to customise the ads that visitors see.'),
      '#default_value' => $config->get('consent_mode_v2_ad_personalization'),
    ];

    $form['#attached']['library'][] = 'axeptio/admin';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fields = [
      'consent_mode_v2',
      'consent_mode_v2_analytics_storage',
      'consent_mode_v2_ad_storage',
      'consent_mode_v2_ad_user_data',
      'consent_mode_v2_ad_personalization',
    ];

    $config = $this->config('axeptio.settings');
    foreach ($fields as $field) {
      $config->set($field, $form_state->getValue($field));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
