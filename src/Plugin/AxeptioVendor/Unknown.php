<?php

namespace Drupal\axeptio\Plugin\AxeptioVendor;

use Drupal\axeptio\AxeptioVendorPluginBase;

/**
 * Plugin Axeptio Unknown vendor.
 *
 * @AxeptioVendor(
 *   id = "unknown",
 *   label = @Translation("Unknown"),
 *   description = @Translation("Unknown vendor."),
 *   sysem_use = true
 * )
 */
class Unknown extends AxeptioVendorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaterns() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getVendorName($source) {
    $parsed = parse_url($source);
    preg_match('/(?:https?:\/\/)?(?:[a-zA-Z0-9]+\.)?([a-zA-Z0-9]+)\.\S{2,}/', $parsed['host'], $matches);

    if (count($matches) == 2) {
      return strtolower($matches[1]);
    }

    return 'unknown';
  }

}
