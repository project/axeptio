<?php

namespace Drupal\axeptio\Plugin\AxeptioVendor;

use Drupal\axeptio\AxeptioVendorPluginBase;

/**
 * Plugin Axeptio Dailymotion vendor.
 *
 * @AxeptioVendor(
 *   id = "dailymotion",
 *   label = @Translation("Dailymotion"),
 *   description = @Translation("Dailymotion vendor.")
 * )
 */
class Dailymotion extends AxeptioVendorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaterns() {
    return ['/dailymotion/'];
  }

}
