<?php

namespace Drupal\axeptio\Plugin\AxeptioVendor;

use Drupal\axeptio\AxeptioVendorPluginBase;

/**
 * Not concerned plugin.
 */
class NotConcerned extends AxeptioVendorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaterns() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultAction() {
    return static::NOT_CONCERNED;
  }

}
