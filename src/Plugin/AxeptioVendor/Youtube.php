<?php

namespace Drupal\axeptio\Plugin\AxeptioVendor;

use Drupal\axeptio\AxeptioVendorPluginBase;

/**
 * Plugin Axeptio YouTube vendor.
 *
 * @AxeptioVendor(
 *   id = "youtube",
 *   label = @Translation("Youtube"),
 *   description = @Translation("Youtube vendor.")
 * )
 */
class Youtube extends AxeptioVendorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaterns() {
    return ['/youtube/'];
  }

}
