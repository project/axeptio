<?php

namespace Drupal\axeptio\Plugin\AxeptioVendor;

use Drupal\axeptio\AxeptioVendorPluginBase;

/**
 * Plugin Axeptio Gmaps vendor.
 *
 * @AxeptioVendor(
 *   id = "gmaps",
 *   label = @Translation("Gmap"),
 *   description = @Translation("Gmap vendor.")
 * )
 */
class Gmaps extends AxeptioVendorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaterns() {
    return ['/^https:\/\/www\.google\.com\/maps\/embed/'];
  }

}
