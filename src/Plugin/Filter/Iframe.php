<?php

namespace Drupal\axeptio\Plugin\Filter;

use Drupal\axeptio\AxeptioIframe;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The filter to hide iframe content before.
 *
 * @Filter(
 *   title = @Translation("Axeptio iframe"),
 *   id = "axeptio_iframe",
 *   description = @Translation("Axeptio iframe."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class Iframe extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * Axeptio Iframe service.
   *
   * @var \Drupal\axeptio\AxeptioIframe
   */
  protected $axeptioIframe;

  /**
   * Constructs a new Iframe object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\axeptio\AxeptioIframe $axeptioIframe
   *   Axeptio Iframe service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AxeptioIframe $axeptioIframe) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->axeptioIframe = $axeptioIframe;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('axeptio.iframe')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    // Extract iframe from text.
    preg_match_all('/<iframe(?:.*)>(.*)<\/iframe>/', $text, $matches);

    foreach ($matches[0] as $iframe) {

      $replacement = $this->axeptioIframe->iframeToAxeptio($iframe);
      if ($replacement) {
        $text = str_replace($iframe, $replacement, $text);
      }
    }

    return new FilterProcessResult($text);
  }

}
