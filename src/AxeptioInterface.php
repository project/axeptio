<?php

namespace Drupal\axeptio;

/**
 * Axeptio service interface.
 */
interface AxeptioInterface {

  /**
   * Get Axeptio config object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The Axeptio config.
   */
  public function getConfig();

  /**
   * Return the Axeptio ID.
   *
   * @return string|null
   *   The Axeptio ID "clientId".
   */
  public function getId();

  /**
   * Indicate if the cookies is language specific.
   *
   * @return bool
   *   Return true if language specific is enabled.
   */
  public function isLanguageSpecific();

  /**
   * Return the Axeptio cookies version setting "cookiesVersion".
   *
   * @return string|null
   *   Return the Axeptio cookie version "cookiesVersion".
   */
  public function getCookiesVersion($admin = FALSE);

  /**
   * Return the Axeptio cookies duration in day.
   *
   * @return int|null
   *   The cookie duration in day "userCookiesDuration".
   */
  public function getCookiesDuration();

  /**
   * Indicate if the cookie holding choices is HTTPS only.
   *
   * @return bool
   *   Return true if HTTPS only "userCookiesSecure".
   */
  public function getCookiesSecure();

  /**
   * Checks if the module is configured.
   *
   * @return bool
   *   Return true if configured.
   */
  public function isConfigured();

  /**
   * Indicate if the Consent Mode V2 is enabled.
   *
   * @return bool
   *   True if the consent mode is enabled.
   */
  public function isConsentModeV2IsEnable();

  /**
   * Indicate if the Analytics Storage is enabled.
   *
   * @return bool
   *   True if the Analytics storage is enabled.
   */
  public function getConsentModeV2AnalyticsStorage();

  /**
   * Indicate if the Ad Storage is enabled.
   *
   * @return bool
   *   True if the AD Storage is enabled.
   */
  public function getConsentModeV2AdStorage();

  /**
   * Indicate if the Ad User Data is enabled.
   *
   * @return bool
   *   True if the Ad User Data is enabled.
   */
  public function getConsentModeV2AdUserData();

  /**
   * Indicate if the Ad Personalization is enabled.
   *
   * @return bool
   *   True if the personalization is enabled.
   */
  public function getConsentModeV2AdPersonalization();

}
