<?php

namespace Drupal\axeptio;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\Mapper\MapperInterface;

/**
 * Interface of AxeptioVendorPluginManager.
 */
interface AxeptioVendorPluginManagerInterface extends FallbackPluginManagerInterface, MapperInterface {

  /**
   * Return the fallback plugin instance.
   *
   * @return \Drupal\axeptio\AxeptioVendorInterface
   *   The fallback plugin instance.
   */
  public function getFallbackPlugin();

}
