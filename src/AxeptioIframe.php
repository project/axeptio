<?php

namespace Drupal\axeptio;

/**
 * Axeptio Iframe service.
 */
class AxeptioIframe {

  /**
   * Axeptio service.
   *
   * @var \Drupal\axeptio\AxeptioInterface
   */
  protected $axeptio;

  /**
   * Axeptio plugin manager.
   *
   * @var \Drupal\axeptio\AxeptioVendorPluginManager
   */
  protected AxeptioVendorPluginManager $axeptioVendorPluginManager;

  /**
   * Constructs a new AxeptioIframe object.
   *
   * @param \Drupal\axeptio\AxeptioInterface $axeptio
   *   The Axeptio main service.
   * @param \Drupal\axeptio\AxeptioVendorPluginManager $axeptioVendorPluginManager
   *   The Axeptio Plugin manager.
   */
  public function __construct(AxeptioInterface $axeptio, AxeptioVendorPluginManager $axeptioVendorPluginManager) {
    $this->axeptio = $axeptio;
    $this->axeptioVendorPluginManager = $axeptioVendorPluginManager;
  }

  /**
   * Convert iframe content to compatible Axeptio format.
   *
   * @param string $iframe
   *   The iframe content.
   */
  public function iframeToAxeptio($iframe) {
    // Extract source from iframe.
    preg_match('/src="((?:https?:\/\/)?(?:\/)?[a-zA-Z0-9]+\.\S{2,})"/', $iframe, $source_matches);
    if (empty($source_matches)) {
      return FALSE;
    }

    $source = $source_matches[0];

    // Get the source vendor.
    $vendor = $this->getVendor($source_matches[1]);

    // Replace the base source per the Axeptio version.
    return str_replace($source, 'src="" data-requires-vendor-consent="' . $vendor->getVendorName($source_matches[1]) . '" data-src="' . $source_matches[1] . '"', $iframe);
  }

  /**
   * Retrieve the vendor of the given source.
   *
   * @param string $source
   *   The source url.
   *
   * @return \Drupal\axeptio\AxeptioVendorInterface
   *   Return the vendor plugin.
   */
  public function getVendor($source) {
    // Search for a matching vendor plugin.
    /** @var \Drupal\axeptio\AxeptioVendorInterface $plugin */
    foreach ($this->axeptioVendorPluginManager->getPlugins() as $plugin) {
      if ($plugin->isSourceIsConcerned($source)) {
        return $plugin;
      }
    }

    // Return the default "unknown" plugin.
    return $this->axeptioVendorPluginManager->getFallbackPlugin();
  }

}
