<?php

namespace Drupal\axeptio\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines axeptio_vendor annotation object.
 *
 * @Annotation
 */
class AxeptioVendor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * Plugin system used.
   *
   * Indicate if the plugin is a system plugin like the default plugin used when
   * no vendor match the source.
   *
   * @var bool
   */
  public $sysem_use = FALSE;

}
