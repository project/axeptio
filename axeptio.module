<?php

/**
 * @file
 * Adds Axeptio to Drupal.
 */

use Drupal\Core\Cache\Cache;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function axeptio_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.axeptio':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_page_attachments().
 */
function axeptio_page_attachments(array &$attachments) {
  /** @var \Drupal\axeptio\Axeptio $axeptio */
  $axeptio = \Drupal::service('axeptio.base');

  // Add module cache tags.
  $config = $axeptio->getConfig();
  $attachments['#cache']['tags'] = Cache::mergeTags($page['#cache']['tags'] ?? [], $config->getCacheTags());

  if ($axeptio->isConfigured()) {
    $attachments['#attached']['library'][] = 'axeptio/base';
    $attachments['#attached']['drupalSettings']['axeptio'] = [
      'clientId' => $axeptio->getId(),
      'userCookiesDuration' => $axeptio->getCookiesDuration(),
      'userCookiesSecure' => $axeptio->getCookiesSecure(),
    ];

    $cookiesVersion = $axeptio->getCookiesVersion();
    if ($cookiesVersion) {
      $attachments['#attached']['drupalSettings']['axeptio']['cookiesVersion'] = $cookiesVersion;
    }

    if ($axeptio->isConsentModeV2IsEnable()) {
      $attachments['#attached']['drupalSettings']['axeptio']['googleConsentMode'] = [
        'default' => [
          'analytics_storage' => $axeptio->getConsentModeV2AnalyticsStorage() ? 'granted' : 'denied',
          'ad_storage' => $axeptio->getConsentModeV2AdStorage() ? 'granted' : 'denied',
          'ad_user_data' => $axeptio->getConsentModeV2AdUserData() ? 'granted' : 'denied',
          'ad_personalization' => $axeptio->getConsentModeV2AdPersonalization() ? 'granted' : 'denied',
          'wait_for_update' => 500,
        ],
      ];
    }
  }
}
