(function ($, Drupal, once) {
  Drupal.behaviors.axeptio = {
    attach(context, settings) {
      once('axeptio', $(context).find('body')).forEach(function () {
        window.axeptioSettings = settings.axeptio;

        (function (d, s) {
          const t = d.getElementsByTagName(s)[0];
          const e = d.createElement(s);
          e.async = true;
          e.src = '//static.axept.io/sdk.js';
          t.parentNode.insertBefore(e, t);
        })(document, 'script');
      });

      // iframe management.
      void 0 === window._axcb && (window._axcb = []);
      window._axcb.push(function (axeptio) {
        axeptio.on('cookies:complete', function (choices) {
          const vendors = {};
          $.each(choices, function (vendor, values) {
            vendors[vendor.toLowerCase()] = values;
          });

          document
            .querySelectorAll('[data-requires-vendor-consent]')
            .forEach(function (el) {
              const vendor = el.getAttribute('data-requires-vendor-consent');
              if (vendors[vendor]) {
                el.setAttribute('src', el.getAttribute('data-src'));
              }
            });
        });
      });
    },
  };
})(jQuery, Drupal, once);
