Module: Axeptio  
Author: Damien LAGUERRE <https://www.drupal.org/user/993490>


Description
===========
Axeptio.eu integration module.

Requirements
============
You have to configure the plugin on https://www.axeptio.eu/

Installation
============
Use composer :

```
composer require drupal/axeptio
```

Usage
=====
Enable and configure the module :

* Go to Configuration > System > Axeptio
* Fill in the Axeptio settings form.
