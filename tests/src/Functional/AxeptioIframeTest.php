<?php

namespace Drupal\Tests\axeptio\Functional;

use Drupal\filter\Entity\FilterFormat;
use Drupal\node\NodeInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 * Axeptio Iframe test class.
 *
 * @group axeptio
 */
class AxeptioIframeTest extends BrowserTestBase {

  use NodeCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'field',
    'filter',
    'system',
    'text',
    'axeptio',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Multiple plugin vendor test.
   */
  public function testAxeptioVendorPlugins() {
    $axeptioIframe = $this->container->get('axeptio.iframe');

    $plugin = $axeptioIframe->getVendor('https://www.youtube.com/embed/foobar');
    $this->assertEquals('youtube', $plugin->getPluginId());

    $plugin = $axeptioIframe->getVendor('https://www.dailymotion.com/embed/video/foobar');
    $this->assertEquals('dailymotion', $plugin->getPluginId());

    $plugin = $axeptioIframe->getVendor('https://www.google.com/maps/embed?pb=');
    $this->assertEquals('gmaps', $plugin->getPluginId());

    $plugin = $axeptioIframe->getVendor('https://www.foobar.com/');
    $this->assertEquals('unknown', $plugin->getPluginId());
  }

  /**
   * Test the iframe content replacement.
   */
  public function testIframe() {
    // Configure the filter format with the Axeptio plugin.
    $format = FilterFormat::create([
      'format' => 'axeptio_test',
      'name' => 'Test format',
      'filters' => [
        'axeptio_iframe' => [
          'id' => 'axeptio_iframe',
          'provider' => 'axeptio',
          'status' => TRUE,
          'weight' => 0,
          'settings' => [],
        ],
        'filter_html' => [
          'id' => 'filter_html',
          'provider' => 'filter',
          'status' => TRUE,
          'weight' => -10,
          'settings' => [
            'allowed_html' => '<br> <p> <iframe src width height>',
          ],
        ],
      ],
    ]);
    $format->save();

    // Create a node with a body field containing an iframe.
    $this->drupalCreateContentType(['type' => 'page']);
    $node = $this->drupalCreateNode([
      'title' => 'Axeptio test node',
      'type' => 'page',
      'body' => [
        'value' => '<p><iframe src="https://www.youtube.com/embed/foobar" width="560" height="315"></iframe></p>',
        'format' => 'axeptio_test',
      ],
      'status' => NodeInterface::PUBLISHED,
    ]);

    // Check if the iframe source is replaced.
    $this->drupalGet($node->toUrl());
    $this->assertSession()
      ->responseContains('<iframe src="" data-requires-vendor-consent="youtube"');
  }

}
