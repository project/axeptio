<?php

namespace Drupal\Tests\axeptio\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the Axeptio plugin.
 *
 * @group axeptio
 */
class AxeptioTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'axeptio',
    'axeptio_test',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Return the default Axeptio settings.
   *
   * @return array
   *   The default Axeptio configuration.
   */
  protected function getDefaultSettingsValues() {
    return [
      'id' => 'MyAxeptioProjectId',
      'cookies_duration' => 111,
      'cookies_secure' => 1,
      'language_specific' => 0,
      'cookies_version' => 'my-axeptio-project',
    ];
  }

  /**
   * Tests that the Axeptio settings page works.
   */
  public function testSettingsForm() {
    $account = $this->drupalCreateUser(['administer axeptio']);
    $this->drupalLogin($account);

    $assertSession = $this->assertSession();
    $this->drupalGet(Url::fromRoute('axeptio.settings_form'));
    $assertSession->pageTextContains('Axeptio settings');

    // Build the default values.
    $form_values = $this->getDefaultSettingsValues();
    $cookies_version = $form_values['cookies_version'];
    unset($form_values['cookies_version']);

    $this->submitForm($form_values, 'Save configuration');

    foreach ($form_values as $key => $value) {
      // Search the field on the form.
      $field = $this->getSession()->getPage()->findField($key);
      $this->assertEquals($value, $field->getValue(), "Test field $key on settings form");
    }

    // Test the cookies version.
    $form_values['cookies_version'] = $cookies_version;
    $this->submitForm($form_values, 'Save configuration');

    $field = $this->getSession()->getPage()->findField('cookies_version');
    $this->assertEquals($form_values['cookies_version'], $field->getValue(), "Test field cookies_version on settings form");
  }

  /**
   * Test the Axeptio plugin settings on page.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testAxeptioSettings() {

    $path = base_path() . $this->getModulePath('axeptio');

    // Check Axeptio library and drupal settings on configured.
    // Set default config.
    $config = $this->config('axeptio.settings');
    foreach ($this->getDefaultSettingsValues() as $key => $values) {
      $config->set($key, $values);
    }
    $config->save();

    $this->drupalGet('');
    $this->assertSession()
      ->responseContains('<script src="' . $path . '/assets/js/axeptio.js');
    $this->assertSession()
      ->responseContains('"axeptio":{"clientId":"MyAxeptioProjectId","userCookiesDuration":111,"userCookiesSecure":true,"cookiesVersion":"my-axeptio-project"}');

    // Check Axeptio library and drupal settings when not configured.
    // Set default config.
    foreach ($this->getDefaultSettingsValues() as $key => $values) {
      $config->set($key, NULL);
    }
    $config->save();

    $this->drupalGet('');
    $this->assertSession()
      ->responseNotContains('"axeptio":{"clientId":"MyAxeptioProjectId","userCookiesDuration":111,"userCookiesSecure":true,"cookiesVersion":"my-axeptio-project"}');
    $this->assertSession()
      ->responseNotContains('<script src="' . $path . '/assets/js/axeptio.js');
  }

}
