<?php

namespace Drupal\axeptio_test\Form;

use Drupal\axeptio\Form\SettingsForm;

/**
 * Override of the Axeptio settings form for test purpose.
 */
class SettingsFormTest extends SettingsForm {

  /**
   * {@inheritdoc}
   *
   * Override the default function to not call the Axeptio endpoint on testing.
   */
  protected function getProjectFromAxeptio(string $id) {
    if ($id === 'MyAxeptioProjectId') {
      return ['my-axeptio-project' => 'my-axeptio-project'];
    }
    return [];
  }

}
