<?php

namespace Drupal\axeptio_test\Routing;

use Drupal\axeptio_test\Form\SettingsFormTest;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber to override default route definition.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Override the default form class.
    if ($route = $collection->get('axeptio.settings_form')) {
      $default = $route->getDefaults();
      $default['_form'] = SettingsFormTest::class;
      $route->setDefaults($default);
    }
  }

}
